package JavaHW3_2;

public class User{
    private String name;
    private Book bookInUse = null;
    private Book[] UsingHistory = new Book[1];

    User(String name) {
        this.name = name;
    }

    public void takeBookFromLibrary(Book bookInUse) {
        this.bookInUse = bookInUse;
        this.addUsingHistory(bookInUse);
    }

    public String getName() {
        return this.name;
    }

    public void returnBookToLibrary( Library myLibrary, String bookTitle) {
                this.bookInUse.setNewGrade();
                this.bookInUse = null;
    }

    private void addUsingHistory(Book newBook) {
        Book[] usingHistory = new Book[this.UsingHistory.length + 1];
        System.arraycopy(this.UsingHistory, 0, usingHistory, 0, this.UsingHistory.length);
        UsingHistory[this.UsingHistory.length] = newBook;
        this.UsingHistory = new Book[UsingHistory.length];
        System.arraycopy(UsingHistory, 0, this.UsingHistory, 0, this.UsingHistory.length);
    }

    public boolean WasBookGetted() {
        if (this.bookInUse != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getBookInUseTitle() {
        return this.bookInUse.getBookTitle();
    }
    public String getBookInUseAuthor(){
        return this.bookInUse.getAuthor();
    }

    public void giveBookToUser(String bookTitle, Library myLibrary) {
        if (!this.WasBookGetted() ) {
            if (!myLibrary.isUnique(bookTitle) && myLibrary.isBookGoted(bookTitle)) {
                this.bookInUse = myLibrary.getBookAtIndex(myLibrary.getBookIndex(bookTitle));
            }
        }
    }
}