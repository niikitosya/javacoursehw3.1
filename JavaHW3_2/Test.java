package JavaHW3_2;

import java.util.SortedMap;

public class Test {
    public static void main(String[] args) {
        Library myLibrary = new Library();
        User user1 = new User("Nikita");
        User user2 =new User("Uliana");
        User user3 =new User("Roman");
        User user4 = new User("Dmitriy");
        myLibrary.addUser(user1);
        myLibrary.addUser(user2);
        myLibrary.addUser(user3);
        Book book1 = new Book("Булгаков", "Мастер и маргарита");
        Book book2 = new Book("Булгаков", "Собачье сердце");
        Book book3 = new Book("Булгаков", "Собачье сердце");
        Book book4 = new Book("Пушкин", "Капитанская дочка");
        System.out.println(" Тест 1: Добавить книги в библиотеку, Книга 3 и Книга 2 одинаковые"+ "\n" );
        myLibrary.addBook(book1);
        myLibrary.addBook(book2);
        myLibrary.addBook(book3);
        myLibrary.addBook(book4);
        myLibrary.showBooks();
        System.out.println();
        System.out.println("Тест 2: Удалить книгу  \"Собачье сердце\" из библиотеки"+ "\n" );
        myLibrary.removeBook("Собачье сердце");
        myLibrary.showBooks();
        System.out.println();
        System.out.println("Тест 3: Попытаемся удалить не существующую книгу"+ "\n" );
        myLibrary.removeBook("Собачье сердце");
        System.out.println();
        System.out.println("Тест 4: Найдем все книги автора, предварительно венув книгу \"Собачье сердце\", выведем результат на экран"+ "\n" );
        myLibrary.addBook(book2);
        Book[] authorBooks = myLibrary.searchByAuthor("Булгаков");
        if(authorBooks != null) {
            for (int i = 0; i < authorBooks.length; i++) {
                System.out.println(authorBooks[i].getBookTitle());
            }
        }
        System.out.println();
        System.out.println("Тест 5: Попытаемся найти книги автора, которые не нредставлены в библиотеке"+ "\n" );
        authorBooks = myLibrary.searchByAuthor("Тургенев");
        if(authorBooks != null) {
            for (int i = 0; i < authorBooks.length; i++) {
                System.out.println(authorBooks[i].getBookTitle());
            }
        }
        System.out.println();
        System.out.println("Тест 6: Найти автора по названию книги\n");
        Book bookForSearch = myLibrary.searchByTitle("Собачье сердце");
        System.out.println(bookForSearch.getAuthor() + " " + bookForSearch.getBookTitle());
        System.out.println();
        System.out.println("Тест 6.1: Найти автора по не существующей книге\n");
        bookForSearch = myLibrary.searchByTitle("Гроза\n");
        System.out.println();
        System.out.println("Тест 7: отдолжим книгу посетителю\n");
        System.out.println("Тест 7.1: у посетителя нет книги, книга в библиотеке\n" );
        myLibrary.giveBookToUser( user1,"Собачье сердце");
        System.out.println("Результат: у пользователя появилась книга?  " + user1.WasBookGetted() + "\nВ библиотеке книга отмечена как одолженная?" + myLibrary.isBookGoted("Собачье сердце")+ "\n"  );
        System.out.println("Тест 7.2: у посетителя есть книга, книга в библиотеке\n" );
        myLibrary.giveBookToUser( user1,"Мастер и маргарита");
        System.out.println("Результат: у пользователя не забрали предыдущую книгу?  " + user1.WasBookGetted()  + "\nВ библиотеке книга отмечена как одолженная?" + myLibrary.isBookGoted("Мастер и маргарита")+ "\n"  );
        System.out.println("Тест 7.3: у посетителя нет, книга в пользовании\n" );
        myLibrary.giveBookToUser( user2,"Собачье сердце");
        System.out.println("Результат: у пользователя появилась книга?  " + user2.WasBookGetted() + "\nВ библиотеке книга отмечена как одолженная? " + myLibrary.isBookGoted("Собачье сердце") );
        System.out.println();
        System.out.println("Тест 7.4: Пользователь не зарегестрирован в библиотеке\n" );
        myLibrary.giveBookToUser(user4, "Мастер и маргарита");
        System.out.println("Выведем на экран пользователей, и книги, которые они взяли");
        System.out.println();
        myLibrary.showUsers();
        System.out.println("Тест 8: Вернуть книгу от пользователя");
        System.out.println("Тест 8.1: посетитель брал не эту книгу\n" );
        myLibrary.returnBookToLibrary(user1,"Мастер и маргарита");
        System.out.println("Результат: у пользователя осталась книга?  " + user1.WasBookGetted() + "\nВ библиотеке книга отмечена как одолженная?" + myLibrary.isBookGoted("Мастер и маргарита") + "\n" );
        System.out.println("Тест 8.2: у пользователя нет книг, книга находится в библиотеке\n" );
        myLibrary.returnBookToLibrary( user2, "Собачье сердце");
        System.out.println("Результат: у пользователя осталась книга? " + user2.WasBookGetted() + "\nВ библиотеке книга отмечена как одолженная? " + myLibrary.isBookGoted("Собачье сердце") + "\n" );
        System.out.println();
        System.out.println("Тест 8.3: у посетителя эта книга\n" );
        myLibrary.returnBookToLibrary(user1, "Собачье сердце");
        System.out.println("Результат: у осталась книга?  " + user1.WasBookGetted() + "\nВ библиотеке книга отмечена как одолженная?" + myLibrary.isBookGoted("Собачье сердце") + "\n" );
        System.out.println();

        System.out.println("Тест 9: рейтинг книги");
        System.out.println("Тест 9.1: рейтинг книги, которая оценена хотя бы раз");
        System.out.println("Рейтинг книги " + book2.getAuthor() + ". " + book2.getBookTitle() + " - " + book2.getRating());
        myLibrary.giveBookToUser(user1, "Собачье сердце");
        myLibrary.returnBookToLibrary(user1, "Собачье сердце");
        System.out.println("Рейтинг книги " + book2.getAuthor() + ". " + book2.getBookTitle() + " - " + book2.getRating());
        myLibrary.showUsers();
    }
}

