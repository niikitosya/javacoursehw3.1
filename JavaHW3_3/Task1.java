package JavaHW3_3;

public class Task1 {
    public static void main(String[] args) {
    new Dolphin().Annotation();
    new Eagle().Annotation();
    }
   
}
abstract class Animal {
    protected String name;
    protected String moveType;
    protected String moveSpeed;

    Animal(String name, String moveType, String moveSpeed) {
        this.name = name;
        this.moveSpeed = moveSpeed;
        this.moveType = moveType;
    }

    public String getName() {
        return this.name;
    }

    public String getMoveType() {
        return this.moveType;
    }

    public String getMoveSpeed() {
        return moveSpeed;
    }

    public final void sleep() {
        System.out.println(name + " спит");
    }

    public final void eat() {
        System.out.println(name + " ест");
    }

    public void moveParams() {
        System.out.print(moveType + " " + moveSpeed);
    }
    public void Annotation(){
        System.out.print(this.getName());
        System.out.print(" ");
        this.moveParams();
        System.out.println();
        this.typeOfBirth();
        System.out.println();
        this.sleep();
        this.eat();
    }

    protected abstract void typeOfBirth();
}
    abstract class Mammals extends Animal{
        Mammals(String name, String moveType, String moveSpeed) {
            super(name, moveType, moveSpeed);
        }
        public final void typeOfBirth(){
            System.out.print(name + " - живородящий");
        }
    }
    abstract class Fish extends Animal {
        Fish(String name, String moveType, String moveSpeed) {
            super(name, moveType, moveSpeed);
        }
        public final void typeOfBirth(){
            System.out.print(name + " - мечет икру");
        }
    }
abstract class Bird extends Animal {
    Bird(String name, String moveType, String moveSpeed) {
        super(name, moveType, moveSpeed);
    }
    public final void typeOfBirth(){
        System.out.print(name + " - откладывает яйца");
    }
}
abstract interface Swimming{
    String moveType = "плавает";
}
abstract interface Flying{
    String moveType = "летает";
}
abstract interface Walking{
    String moveType = "ходит";
}
class Dolphin extends Mammals implements Swimming {
    Dolphin() {
        super("Дельфин", Swimming.moveType, "быстро");
    }
}
class GoldFish extends Fish implements Swimming{
    GoldFish(){
        super("Золотая рыбка", Swimming.moveType, "медленно");
    }
}
class Eagle extends Bird implements Flying{
    Eagle(){
        super("Орел", Flying.moveType, "быстро");
    }
}
class Bat extends Mammals implements Flying{
    Bat(){
        super("Летучая мышь", Flying.moveType, "медленно");
    }
}



