package JavaHW3_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DogsShow show = new DogsShow();
        show.Registration(scanner.nextInt());
        System.out.println(show.showResult());
    }


}
    class DogsShow{
    private ArrayList<DogOwner> competitors = new ArrayList<>();
    private DogOwner[] winners = new DogOwner[3];

    public void Registration(int k){
        for (int i = 0; i < k; i++){
            competitors.add(new DogOwner());
        }
        for (DogOwner e: competitors){
            e.RegistrationDog();
        }
        for (DogOwner e: competitors){
            e.dog.setGrades();
        }

    }
    public String showResult(){
        double max;
        int index;
        for(int i = 0; i < 3; i++) {
            max = competitors.get(i).dog.getResult();
            index = i;
            for (int j = i; j < competitors.size(); j++){
                if (competitors.get(j).dog.getResult() > max){
                    max = competitors.get(j).dog.getResult();
                    index = j;
                }
                winners[i] = competitors.get(index);
                competitors.set(index, competitors.get(i));
                competitors.set(i, winners[i]);
            }
        }
        String out = "";
        for (DogOwner e: winners){
            out += e.getName() + ": " + e.dog.getDogName() + ", " + e.dog.getResult() + "\n";
        }
        return out;
    }


    }
    class DogOwner {
        Scanner scanner = new Scanner(System.in);
        private String name;
        protected Dog dog;
        DogOwner(){
            this.name =scanner.nextLine();
        }
        public String getName(){
            return name;
        }
        public void RegistrationDog(){
            dog = new Dog();
        }
    }
    class Dog{
        Scanner scanner = new Scanner(System.in);
        String dogName;
        int[] grades = new int[3];
        double result;
        Dog(){
            this.dogName = scanner.nextLine();
        }
        public String getDogName(){
            return dogName;
        }
        public void setGrades(){
            double sum = 0;
            for(int e: grades){
                e = scanner.nextInt();
                sum+=e;
            }
            result = ((int)(sum * 10 /3.0)) / 10.0;
        }
        public double getResult(){
            return result;
        }
    }
