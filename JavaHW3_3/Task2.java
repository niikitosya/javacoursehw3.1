package JavaHW3_3;

import java.util.ArrayList;
import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) {
        showResult(new Chair());
        showResult(new Table());
    }
    public static void showResult(Furniture a){
        System.out.print("Изделие \"" + a.getName() + "\" может быть починена в этой мастерской? - ");
        System.out.print(BestCarpenterEver.CanBeRepaired(a) ? "Да" : "Нет");
        System.out.println();
    }
}
    abstract class BestCarpenterEver {

        public static boolean CanBeRepaired(Object a){
            return  a instanceof Chair;
                }
    }
abstract class Furniture{
    private String name;
    Furniture(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
}
class Chair extends Furniture{
    Chair(){
        super("табуретка");
    }
}
class Table extends Furniture{
    Table(){
        super("стол");
    }
}
