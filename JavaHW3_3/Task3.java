package JavaHW3_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> arrayList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите через пробел колличество столбцов и строк требуемого массива");
        int columns = scanner.nextInt();
        int lines = scanner.nextInt();
        ArrayList<Integer> dif;
        if (!(columns > 100 || columns < 0 || lines > 100 || lines < 0)) {
            for (int i = 0; i < columns; i++) {
                arrayList.add(new ArrayList<>());
                dif = arrayList.get(i);
                for (int j = 0; j < lines; j++) {
                    dif.add(i + j);
                }
            }
            for (int i = 0; i < lines; i++) {
                for (int j = 0; j < columns; j++) {
                    System.out.print(arrayList.get(j).get(i) + " ");
                }
                System.out.println();
            }
        }
    }
}