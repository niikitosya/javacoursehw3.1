package Prrofil_HW2.Task2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.nextLine();
        String second = scanner.nextLine();
        System.out.println(Anagram.isAnagram(first, second));
    }

}
class Anagram {
    public static boolean isAnagram(String str1, String str2) {
        Map<Character, Integer> in = new HashMap<>();
        // удалим пробелы, они нам не нужны
        str1 = str1.replaceAll(" ", "");
        str1 = str1.toLowerCase();
        str2 = str2.replaceAll(" ", "");
        str2 = str2.toLowerCase();

        if (str2.length() != str1.length()) {
            return false;
        }
        for (int i = 0; i < str2.length(); i++) {
            int counter = 1;

            if (in.containsKey(str1.charAt(i))) {
                counter = in.get(str1.charAt(i));
                counter++;
            }
            in.put(str1.charAt(i), counter);

            counter = -1;
            if (in.containsKey(str2.charAt(i))) {
                counter = in.get(str2.charAt(i));
                counter--;
            }
            in.put(str2.charAt(i), counter);
        }

        in.values().removeIf(value -> value == 0);
        return in.size() == 0 ? true : false;
    }
}