package Prrofil_HW2.Task1;

import Training_AbsClasses_Interfaces.Task9.Circle;
import Training_AbsClasses_Interfaces.Task9.GeometricObjects;
import Training_AbsClasses_Interfaces.Task9.Rectangle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ArrayList<GeometricObjects> list= new ArrayList<>();
        list.add(new Circle(3));// вернется
        list.add(new Circle(3));//вернется
        Circle circle =new Circle(2);//вернется
        Circle circle2 = circle;
        list.add(circle);//вернется
        list.add(circle2);//не вернется
        list.add(new Rectangle(4, 2));//вернется
        list.add(new Rectangle(1, 1));//вернется
        toStringArray(getUniqueElements(list));
    }
    public static <T> ArrayList<T> getUniqueElements(ArrayList<T> list){
        Set<T> dif = new HashSet<T>();
        for (T e: list){
            dif.add(e);
        }
        ArrayList<T> out = new ArrayList<>();
        for (T e: dif){
            out.add(e);
        }
        return out;
    }
    public static <T> void toStringArray(ArrayList<T> list){
        for (T e: list){
            System.out.println(e.toString());
        }
    }
}
