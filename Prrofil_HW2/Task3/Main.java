package Prrofil_HW2.Task3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        set1.add(7);
        set1.add(4);
        set1.add(3);
        set2.add(3);
        set2.add(4);
        set2.add(5);
        set2.add(5);
        set2.add(6);
        System.out.println(PowerfulSet.intersection(set1, set2).toString());
        System.out.println(PowerfulSet.union(set1, set2).toString());
        System.out.println(PowerfulSet.relativeComplement(set1, set2));
    }
}
