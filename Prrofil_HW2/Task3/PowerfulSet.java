package Prrofil_HW2.Task3;

import java.util.HashSet;
import java.util.Set;

public final class PowerfulSet {
    public static <T>Set<T> intersection(Set<T> set1, Set<T> set2){
        Set<T> out = new HashSet<>();
        out.addAll(set1);
        out.addAll(set2);
       out.removeIf(value -> !(set1.contains(value) && set2.contains(value)));
        return out;
    }
    public static <T> Set<T> union(Set<T> set1, Set<T> set2){
        Set<T> out = new HashSet<>();
        out.addAll(set1);
        out.addAll(set2);
        return out;
    }
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2){
        Set<T> out = new HashSet<>();
        out.addAll(set1);
        out.removeIf(value -> set2.contains(value));
        return out;
    }
}
