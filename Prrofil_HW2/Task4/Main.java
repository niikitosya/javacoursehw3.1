package Prrofil_HW2.Task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ArrayList<Document> docs = new ArrayList<>();
        try {

            docs.add(new Document(1, "First Book", 25));
            docs.add(new Document(2, "Second Book", 125));
            docs.add(new Document(3, "Third Book", 125));
            docs.add(new Document(1, "Last Book", 25)); //Тест исключения, програма не должна прерваться, а новый документ не появится в листе

        }
        catch (ThisIdAlreadyInUse e){
            System.err.println(e.getMessage());
        }
        for (Document e: docs) {
            System.out.println(e.toString());
        }
        Map<Integer, Document> newDocs = organizeDocuments(docs);
        newDocs.forEach((key, value) -> System.out.println("Key: " + key + "\nValue: \n" + value));

    }

    public static Map<Integer, Document> organizeDocuments(List<Document> docs){
        Map<Integer, Document> out = new HashMap<>();
        for (Document e: docs) {
            out.put(e.getId(), e);
        }
        return out;
    }
}
