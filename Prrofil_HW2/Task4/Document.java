package Prrofil_HW2.Task4;

import java.util.ArrayList;

public class Document {
    private int id  = -22224;
    private String name;
    private int pageCount;
    private static ArrayList<Integer> idPool= new ArrayList<>();
    Document(){
    }
    Document(int id,String name, int pageCount) throws  ThisIdAlreadyInUse{
        if (idPool.size() != 0 && idPool.contains(id)){
            throw new ThisIdAlreadyInUse("Aйди " + id + " занято, присвойте другой айди при повторном создании книги " + name);
        }
            this.id = id;
            idPool.add(id);
            this.name = name;
            this.pageCount = pageCount;

    }


    public int getId(){
        return this.id;
    }

    public void setId(int id)  throws  ThisIdAlreadyInUse{
        if (idPool.size() == 0 || idPool.contains(id)){
            throw new ThisIdAlreadyInUse("Это айди занято");
        }
       if (this.id != -22224){
        idPool.remove(this.id);
       }
        this.id = id;
       idPool.add(this.id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
    public String toString(){
        return (" Id: " + this.id + "\n Name: " + this.name + "\n Pages count: " + this.pageCount + "\n");
    }
}
