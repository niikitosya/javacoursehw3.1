package JavaHW2_1;

import java.util.Scanner;
/* На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
После этого передается число M.

Необходимо найти в массиве число, максимально близкое к M (т.е. такое число, для которого |ai - M|
минимальное). Если их несколько, то вывести максимальное число.
*/
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] matr = new int[n];
        for (int i = 0; i < matr.length; i++) {
            matr[i] = scanner.nextInt();
        }
        int X = scanner.nextInt();
        int diff =Math.abs(matr[0] - X);
        int index= 0;
        for(int i = 1; i < matr.length; i++) {
            if(diff >= (int)Math.abs(matr[i] - X)) {
                diff = (int)Math.abs(matr[i] - X);
                index = i;
            }
        }
        System.out.println(matr[index]);
    }
}
