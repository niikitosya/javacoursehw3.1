package JavaHW2_1;
import java.util.Scanner;

import static java.lang.System.currentTimeMillis;
/* 7.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N
элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного возведением в квадрат каждого элемента, упорядочить
элементы по возрастанию и вывести их на экран.
*/
//т.е. число операций должно быть n*k, где k - число элементов в массиве
// предложение - делить массивы на массивы 2х2, а потом сравнивать

public class Task2_dop {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Введите число в массиве: ");
        int n = scanner.nextInt();
        double[] array = new double[n];
        System.out.print("Введите элементы массива через пробел: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextDouble();
        }
        long timeNow = currentTimeMillis();
        double[] array2 = sort(array);
        System.out.println("Решение заняло " + (currentTimeMillis() - timeNow) + "милисекунд. ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array2[i] + " ");
        }
    }
    public static double[] sort(double[] array) {
        double dif, peremennaya;
        double key = 10;
        int index = 0;

        if (array.length > 3) {
            double[] subarray1 = new double[array.length / 2];
            double[] subarray2 = new double[array.length / 2];
            if (array.length % 2 == 1) {   // если число элементов массива нечетное, мы достанем одно и будем сравнивать их отдельно
                key = array[array.length / 2];
                for (int i = 0; i < array.length / 2; i++) {
                    subarray1[i] = array[i];
                    subarray2[i] = array[array.length / 2 + i + 1];

                }
            } else {
                for (int i = 0; i < array.length / 2; i++) {
                    subarray1[i] = array[i];
                    subarray2[i] = array[array.length / 2 + i];
                }

            }
            subarray1 = sort(subarray1);
            subarray2 = sort(subarray2);
            for (int i = 0; i < subarray1.length; i++) {  // Здесь мы добиваемся того, чтобы в 1 "подмассиве" были меньше соответсвующих чисел второго подмассива
                if (subarray1[i] > subarray2[i]) {
                    dif = subarray1[i];
                    subarray1[i] = subarray2[i];
                    subarray2[i] = dif;
                }
            }
            int m = 0;
            int indexOfSubarray = 0;
            int rounds = array.length;
            if (array.length % 2 == 1) rounds = array.length - 1;
            while (m + indexOfSubarray < rounds) {
                array[m + indexOfSubarray] = subarray1[m];
                while (true) { // главный массив заполяется первым подмассивом, а потом сравнивается элементами второго и они "вклиниваются в основной массив, если они меньше"

                    if (subarray1[m] > subarray2[indexOfSubarray]) {
                        array[m + indexOfSubarray] = subarray2[indexOfSubarray];
                        indexOfSubarray++;
                        array[m + indexOfSubarray] = subarray1[m];
                    }
                    else break;
                }
                m++;
                if (m == subarray2.length) { // заполянем остсаток массива не задействованными ячейками второго массива
                    for (int i = indexOfSubarray; i < subarray2.length; i++) {
                        array[i + m] = subarray2[i];
                    }
                    break;
                }
            }

        if (array.length % 2 == 1) { //определили индекс
            for (int i = 0; i < array.length; i++) {
                if (key < array[i]) {
                    index = i;
                    break;
                } else index = i;
            }

            //вставить средний элемента массива на свое место
            dif = array[index];
            array[index] = key;
            for (int i = index + 1; i < array.length; i++) {
                peremennaya = array[i];
                array[i] = dif;
                dif = peremennaya;
            }
            if (index == array.length - 1) array[array.length - 1] = key;

        }

            return array;
        }
        else {
            if (array.length == 2) { // простое решение
                return sort2(array);
                }
            else return sort3(array);
                }
            }
            public static double[] sort2(double[] array) {
                double dif;
                if (array[0] > array[1]) {
                    dif = array[1];
                    array[1] = array[0];
                    array[0] = dif;
                }
                return array;
            }
            public static double[] sort3(double[] array) { // также простое решение
                    double dif;
                    if (array[0] > array[1]) {
                        dif = array[1];
                        array[1] = array[0];
                        array[0] = dif;
                    }
                        if (array[0] > array[2]) {
                            dif = array[2];
                            array[2] = array[1];
                            array[1] = array[0];
                            array[0] = dif;
                        }
                         if (array[1] > array[2]) {
                            dif = array[2];
                            array[2] = array[1];
                            array[1] = dif;
                        }
                    return array;
    }
}