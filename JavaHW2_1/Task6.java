package JavaHW2_1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] morzeMap ={".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        String word = scanner.nextLine();
        for (int i = 0; i < word.length(); i++) {
            System.out.print(morzeMap[(int)(word.charAt(i)) - 1040]);
            if (i != word.length() - 1) System.out.println(" ");
        }
    }

}
