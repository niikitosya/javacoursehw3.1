package JavaHW2_1;

import java.util.Scanner;
import java.util.Arrays;

    /*Создать программу генерирующую пароль.
    На вход подается число N — длина желаемого пароля. Необходимо проверить, что N >= 8,
    иначе вывести на экран "Пароль с N количеством символов небезопасен" (подставить вместо N число)
    и предложить пользователю еще раз ввести число N. */

public class Task1_dop {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = -144444;

        do {
            if (n != -144444) System.out.println("Пароль с " + n + " количеством символов небезопасен");
            System.out.print("Введите число символов:");
            n = scanner.nextInt();
        }
        while (n < 8);
        System.out.println("Ваш сгенерированный пароль: " + passwordGen(n));

    }

    //Генератор паролей - задумка - нужно 4 вида символов Строчные, заглавные, цифры, спец символы.
    //1. пароль будет храниться в массиве с числом ячеек n. массив пустой стр, значит ячейки пустые ("")
    //2. выбираем колличество строчных букв на рандоме, для этого генерируем число от 1 до (n -3).
    //3. на рандоме генерируем символы и вставляем в рандомную ячейку
    //4. дальше заглавных рекурсия n = n - число строчных букв и т. д.
    //5ю Заполняем массив в рандомное место, если значение оно равно "".
    public static String passwordGen(int n) {
        char[] array = new char[n];
        Arrays.fill(array, '^'); // заполняем символом который не нужен, т.к. пустой массив char заполен 0, а это число
        int k = 4; // колличество видов символов, которые надо сгенерить
        int g; // случайный номер ячейки
        int random = (int) (Math.random() * (n - k + 1)) + 1; // сколько будет строчных букв
        for (int i = 0; i < random; i++) {

            while (true) {

                g = (int) (Math.random() * array.length);
                if (array[g] == '^') {
                    array[g] = (char) ((int) (Math.random() * 26) + 97);
                    break;
                }
            }
        }
        return passwordGen(array, n - random, k - 1);
    }

    public static String passwordGen(char[] array, int n, int k) {
        int g, type = 0, range = 0;
        if (k == 0) { //простой случай, выводим пароль

            String str = "";
            for (int i = 0; i < array.length; i++) {

                str = str.concat(String.valueOf(array[i]));

            }
            System.out.println(str);
            return str;
        }
        if (k == 3) { //Заглавные
            type = 65;
            range = 26;
        }
        if (k == 2) { //Цифры
            type = 48;
            range = 10;
        }
        if (k == 1) { //спецсимволы, для него создам отдельную мапу, они разбросаны и числа никак не связать
            char[] chari = new char[]{'-', '_', '*'};
            for (int i = 0; i < n; i++) {

                while (true) {
                    g = (int) (Math.random() * array.length);
                    if (array[g] == '^') {
                        array[g] = chari[(int) (Math.random() * 3)];
                        break;
                    }
                }
            }
            passwordGen(array, k, 0);
        }
        int random = (int) (Math.random() * (n - k + 1)) + 1;
        for (int i = 0; i < random; i++) {

            while (true) {

                g = (int) (Math.random() * array.length);
                if (array[g] == '^') {
                    array[g] = (char) ((int) (Math.random() * range) + type);
                    break ;
                }
            }
        }
        return passwordGen(array, n - random, k - 1);
    }
}

