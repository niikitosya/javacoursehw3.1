package JavaHW2_1;

import java.util.Scanner;
import java.util.Arrays;
public class Task2 {
        /*
2.	(1 балл) На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого аналогично передается второй массив (aj) длины M.

Необходимо вывести на экран true, если два массива одинаковы
(то есть содержат одинаковое количество элементов и для каждого i == j элемент ai == aj).
Иначе вывести false.
         */

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int[] a= new int[n];
            matrixFiller(a);
            n = scanner.nextInt();
            int[] b = new int[n];
            matrixFiller(b);
            System.out.println(Arrays.equals(a, b));
    }
    public static void matrixFiller (int[] k) {
        Scanner scanner = new Scanner(System.in);
            for (int i = 0; i < k.length;i++) {
                k[i] = scanner.nextInt();
            }
    }
}
