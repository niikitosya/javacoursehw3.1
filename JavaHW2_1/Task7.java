package JavaHW2_1;

import java.util.Scanner;

/* 7.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N
элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного возведением в квадрат каждого элемента, упорядочить
элементы по возрастанию и вывести их на экран.
*/
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k;
        int[] array = new int[n];
        for(int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
            k = array[i];
            array[i] = (int) Math.pow(k, 2);
        }
        int[] array2 = bubble(array);
        for (int i = 0; i < array2.length; i++ ) {
            System.out.print(array2[i]);
            if (i != array2.length -1) System.out.print(" ");
        }
    }
    public static int[] bubble(int[] mas) {
        int diff;
        for (int i = 0; i < mas.length; i++) {

            for (int j = i; j < mas.length; j++) {
                if (mas[i] > mas[j]) {
                    diff = mas[i];
                    mas[i] = mas[j];
                    mas[j] = diff;
                }
            }
        }
        return mas;
    }
}
