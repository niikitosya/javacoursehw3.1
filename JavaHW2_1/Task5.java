package JavaHW2_1;

import java.util.Scanner;

/* 5.	(1 балл) На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов. После этого передается число M — величина сдвига.

Необходимо циклически сдвинуть элементы массива на M элементов вправо.
*/
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] matrix = new int[n];
        int[] newMatrix = new int[n];

        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = scanner.nextInt();
        }
        int k = scanner.nextInt();
            for (int i = 0; i < matrix.length; i++) {
                newMatrix[(i + k) % n] = matrix[i];
            }
               for( int i = 0; i < newMatrix.length - 1; i++) {
                   System.out.print(newMatrix[i] + " ");
               }
                System.out.print(newMatrix[n-1]);


        }
    }

