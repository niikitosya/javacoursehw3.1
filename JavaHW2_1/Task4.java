package JavaHW2_1;
import java.util.Scanner;
 /* 4.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов,
 отсортированный по возрастанию.

Необходимо вывести на экран построчно сколько встретилось различных элементов. Каждая строка должна содержать количество
элементов и сам элемент через пробел.
*/
public class Task4 {
     public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
         int n = scanner.nextInt();
         int[] ai = new int[n];
         for (int i = 0; i < ai.length; i++) {
             ai[i] = scanner.nextInt();
         }
         // создаем двумерный массив, в котором первый массив это цифры, а второй это количество совпадений
         int[][] matches = new int[2][n];
         for (int i = 0; i < ai.length; i++) {
             // проверяем, была ли уже такая, если да то +1 к совпадениям этого числа
             if (linearSearch(matches[0], ai[i]) >= 0) {
                 matches[1][linearSearch(matches[0], ai[i])]++;

             }
             // если не было, выделяем для нее новый элемент массива
             else {
                 Search:
                 for (int j = 0; j < matches[0].length; j++) {
                     if ((matches[0][j] == 0) && (matches[1][j] == 0)) {
                         matches[0][j] = ai[i];
                         matches[1][j] = 1;
                         break Search;
                     }
                 }
             }
         }
         for (int i = 0; i < matches[0].length; i++) {
             if ((matches[0][i] == 0) && (matches[1][i] == 0)) break;
             System.out.println(matches[1][i] + " " + matches[0][i]);
         }
     }

     public static int linearSearch(int[] mass, int k) {
         //return a matrix index
         int g = -2;
         for (int i = 0; i < mass.length; i++) {
             if (k == mass[i]) {
                 g = i;
             }

         }
         return g;
     }
 }