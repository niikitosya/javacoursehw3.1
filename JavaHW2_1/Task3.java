package JavaHW2_1;
import java.util.Scanner;


/* 3.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов,
       отсортированный по возрастанию. После этого вводится число X — элемент, который нужно добавить в массив,
       чтобы сортировка в массиве сохранилась.

        Необходимо вывести на экран индекс элемента массива, куда нужно добавить X.
        Если в массиве уже есть число равное X, то X нужно поставить после уже существующего.
*/


public class Task3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] ai = new int[N];
        for (int i = 0; i < ai.length; i++){
            ai[i] = scanner.nextInt();
        }
        int X = scanner.nextInt();
        for(int i = 0; i < ai.length; i ++) {
            if (X < ai[i]) {
                System.out.println(i);
                break; }
            else if (X == ai[i] && X != ai[i+1]) {
                System.out.println(i + 1);
                break;
            }
        }
    }

}
