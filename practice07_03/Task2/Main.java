package practice07_03.Task2;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(1);
        set1.add(3);
        set1.add(2);
        set1.add(2);
        set1.add(4);
        System.out.println(set1);
        Set<Integer> set2 = new HashSet<>();
        set2.add(2);
        set2.add(3);
        set2.add(4);
        set2.add(5);
        System.out.println(set2);
        set1.retainAll(set2);
        System.out.println(set1);
        set1.forEach(element -> System.out.println(element));
        // Вернет true, если нет общих элементов
        System.out.println(Collections.disjoint(set1, set2));
    }
}
