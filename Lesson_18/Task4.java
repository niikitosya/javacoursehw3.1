package Lesson_18;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int[][] array = new int[n][k + 1];
        int sum;
        int index = 0;
        for (int i = 0; i < n; i++){
            sum = 0;
            for (int j = 0; j < k;j++){
                array[i][j] = scanner.nextInt();
                sum += array[i][j];
            }
            array[i][k] = sum;
            if (array[index][k] <= array[i][k]){
                index = i;
            }
        }
        System.out.println((index + 1) + " " + array[index][k]);
    }
}
