create table public.clients(
    id          serial primary key,
    name        varchar(30),
    phone       varchar(16) unique
);
create table public.flowers(
    id          serial primary key,
    flower_name        varchar(30),
    price       int
);
create table public.orders
(
    id          serial primary key,
    client_id   serial references clients(id),
    flower_id   serial references flowers(id),
    count       int
                constraint invalid_count check (count between 1 and 1000),
    total       int,
    order_date  timestamp not null
);
insert into clients (name, phone)
values ('George', '+7(954)432-32-22');
insert into clients (name, phone)
values ('Anton', '+7(911)411-32-22');
insert into clients (name, phone)
values ('Aleksandr', '+7(911)111-11-22');
insert into flowers(flower_name, price)
VALUES ('роза', 100);
insert into flowers(flower_name, price)
VALUES ('лилия', 50);
insert into flowers(flower_name, price)
VALUES ('ромашка', 25);
select * from orders;
insert into orders(client_id, flower_id, count, total, order_date)
values (1, 1, 10, 1000, date_trunc('second', now() - interval'2 month'));
insert into orders(client_id, flower_id, count, total, order_date)
values (1, 2, 20, 1000, date_trunc('second', now() - interval'4 hour'));
insert into orders(client_id, flower_id, count, total, order_date)
values (3, 1, 10, 1000, date_trunc('second', now() - interval'24 hour'));
insert into orders(client_id, flower_id, count, total, order_date)
values (1, 1, 10, 1000, date_trunc('second', now() - interval'24 month'));
insert into orders(client_id, flower_id, count, total, order_date)
values (2, 3, 10, 750, date_trunc('second', now() - interval'2 year'));
insert into orders(client_id, flower_id, count, total, order_date)
values (1, 3, 10, 750, date_trunc('second', now() - interval'24 day'));
        ------ Задание 1---------

select
    orders.id as номер_заказа,
    flower_name as цветы,
    count as колличество,
    total as итого,
    order_date as дата_заказа,
    name as Имя_покупателя,
    phone as контакты
from
    orders
join public.flowers c on orders.flower_id = c.id
right outer join public.clients on clients.id = orders.client_id;

        ------ Задание 2---------

select
    orders.id as номер_заказа,
    flower_name as цветы,
    count as колличество,
    total as итого,
    order_date as дата_заказа
    from orders
join flowers o on orders.flower_id = o.id
where (client_id = 1) and (order_date > (now() - interval'1 month'))
order by orders.id;

            ------ Задание 3---------

select
    flower_name as цветы,
    count as колличество
from orders
join flowers o on orders.flower_id = o.id
order by count desc
    fetch first 1 row only;

            ------ Задание 4-------
select sum(price * orders.count)
from orders
join flowers fl on fl.id = orders.flower_id
;