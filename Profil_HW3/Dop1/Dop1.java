package Profil_HW3.Dop1;

public class Dop1 {
    public static void main(String[] args) {
        System.out.println(isValidBrackets("(((()))"));
        System.out.println(isValidBracketsVerTwo("(((()))"));
    }
    public static boolean isValidBrackets(String str){
        if (str.length() == 0) return true;
        if (str.charAt(0) == ')' || str.length() %2 != 0) return false;
        String def = str.replaceAll("\\(\\)", "");
        return isValidBrackets(def);
    }
    public static boolean isValidBracketsVerTwo(String str){
        int count = 0;
        for (int i = 0; i < str.length(); i++){
            switch (str.charAt(i)){
                case '(' -> count++;
                case ')' -> count--;
            }
            if (count < 0) return false;
        }
        if (count == 0) return true;
        else return false;
    }
}

