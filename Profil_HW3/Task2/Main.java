package Profil_HW3.Task2;

public class Main {

    public static void main(String[] args){
        TestClassWithAnnotation a = new TestClassWithAnnotation();
        TestClass b = new TestClass();
        System.out.println("Тест 1: аннотация есть");
        IsAnnotatedWithStoredValues.IsAnnotated(a.getClass());
        System.out.println("Тест 2: аннотации нет");
        IsAnnotatedWithStoredValues.IsAnnotated(b.getClass());
    }
}
