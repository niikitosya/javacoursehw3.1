package Profil_HW3.Task2;

import Profil_HW3.Task1.IsLike;
// для проверки любой анотрации
public class IsAnnotatedWithStoredValues {
    public static void IsAnnotated(Class<?> clazz) {
        if (clazz.isAnnotationPresent(IsLike.class)){
            System.out.println(true);
            System.out.println("Value: " + clazz.getAnnotation(IsLike.class).value());
        }
        else {
            System.out.println(false);
        }
    }

    //для проверки @ISLike

}
