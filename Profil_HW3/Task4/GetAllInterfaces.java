package Profil_HW3.Task4;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GetAllInterfaces {
    public static void getItAll(Class<?> clazz) {
        Set<Class<?>> out = new HashSet<>();
        do {
            out.add(clazz.getSuperclass());
            for (Class<?> e : clazz.getInterfaces()) {
                out.add(e);
                out.addAll(List.of(e.getInterfaces()));
            }
            clazz = clazz.getSuperclass();
            out.add(clazz);
        }
        while (!clazz.equals(Object.class));
        for (Class<?> forPrinting: out){
            System.out.println( (forPrinting.isInterface() ? "Interface: ": "Class: ") + forPrinting.getSimpleName());
        }
    }
}
