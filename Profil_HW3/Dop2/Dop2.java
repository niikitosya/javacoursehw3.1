package Profil_HW3.Dop2;

import java.util.Set;

public class Dop2 {
    public static void main(String[] args) {
        System.out.println(isValidBrackets("[{(){}}][()]{}"));
    }
    public static boolean isValidBrackets(String str){
        int[] count = {0, 0, 0};
        for (int i = 0; i < str.length(); i++){
            switch (str.charAt(i)){
                case '(' -> count[0]++;
                case ')' -> count[0]--;
                case '{' -> count[1]++;
                case '}' -> count[1]--;
                case '[' -> count[2]++;
                case ']' -> count[2]--;
            }
            if (count[0] < 0 || count[1] < 0 || count[2] < 0) return false;
        }
        if (count[0] == 0 && count[1] == 0 && count[2] == 0) return true;
        else return false;
    }
}
