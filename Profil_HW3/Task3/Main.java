package Profil_HW3.Task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        try {
            Method method = APrinter.class.getDeclaredMethod("print", int.class);
            method.invoke(new APrinter(),  2);
        }
       catch (NoSuchMethodException e) {
            System.err.println("Ошибка! Метода с данным названии не существует в данном классе");
        }
        catch (IllegalArgumentException e) {
            System.err.println("Перередан аргумент недопустимого типа");
        } catch (InvocationTargetException e) {
            System.err.println("Ошибка! Метода с данным названии не существует в данном классе");
        } catch (IllegalAccessException e) {
            System.err.println("Ошибка доступа");
        }
    }
}
