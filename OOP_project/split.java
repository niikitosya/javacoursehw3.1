package OOP_project;

public class split {
    public static void main(String[] args) {

       String[] a =  splitt("ab#12#453", "#");
        showString(a);
    }
    public static String[] splitt(String s, String regex){

        String[] out = new String[s.length()];
        int k = 0;
        int lastMatch = 0;
        for (int i = 0; i < s.length(); i++){
            if (s.substring(i, i + regex.length()).equals(regex)){
                out[k] = s.substring(lastMatch, i);
                k++;
                out[k] = regex;
                k++;
                i += regex.length();
                lastMatch = i;
            }
            if (lastMatch != s.length()) {
                out[k] = s.substring(lastMatch, s.length());
            }
        }
        return out;
    }
    public static void showString(String[] a){
        System.out.print(a[0]);
        for (int i = 1; i < a.length;i++){
            if (a[i] != null)
            System.out.print(", " + a[i]);
        }
    }
}
