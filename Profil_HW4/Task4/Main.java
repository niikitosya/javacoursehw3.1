package Profil_HW4.Task4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Comparator<Integer> comparator = ((o1, o2) -> o1 > o2 ? -1 : o1 < 02 ? 1 : 0);
        List<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(100);
        nums.add(3);
        nums.add(4);
        nums.add(-10);
        nums.stream().sorted(comparator).forEach(System.out :: println);
        }
    }

