package Profil_HW4.Task6;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> superSet = new HashSet<>();
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        Set<Integer> set3 = new HashSet<>();
        for (int i = 1; i <=100; i++){
            switch (i % 3){
                case 0 -> set1.add(i);
                case 1 -> set2.add(i);
                case 2 -> set3.add(i);
            }
        }
        superSet.add(set2);
        superSet.add(set1);
        superSet.add(set3);
        Set<Integer> out = superSet.stream()
                .flatMap(Set ::stream)
                .collect(Collectors.toSet());
        out.stream().forEach(i -> {
            if (i % 10 == 1 && i != 1){
                System.out.println();
            }
            System.out.print(i + " ");
        });
    }
}
