package Profil_HW4.Task5;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("hi");
        list.add("fjkg");
        list.add("bad");
        list.add("good");
        System.out.println(String.join(", ", list.stream().map(String::toUpperCase).toList()));
    }
}
