package Profil_HW4.Task3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("");
        list.add("Hi");
        list.add("World");
        list.add("");
        long count = list.stream().filter(a -> a.length() != 0)
                .count();
        System.out.println(count);
    }
}
