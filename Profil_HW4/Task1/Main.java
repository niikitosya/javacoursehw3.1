package Profil_HW4.Task1;

import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> list= new ArrayList<>();
        for (int i = 1; i <= 100; i++){
            list.add(i);
        }
        int sum = list.stream().filter(i -> i % 2 == 0)
                .mapToInt(Integer:: intValue)
                .sum();
        System.out.println(sum);
    }
}
