package Profil_HW4.Task2;

import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        int out = list.stream()
                .reduce(1, (a, b) -> a*b);
        System.out.println("Результат умножения " + list.toString().replaceAll(", ", " * ").replaceAll("[\\[\\]]", "") + " = " + out);
    }
}
