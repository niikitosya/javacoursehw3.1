package Training_AbsClasses_Interfaces.Task9;

import java.util.Date;

//Измените класс GeometricObject, чтобы реализовать интерфейс Comparable и определить статический метод max() в классе
// GeometricObject для поиска наибольшего из двух объектов типа GeometricObject. Нарисуйте UML-диаграмму и реализуйте новый
// класс GeometricObject. Напишите тестовую программу, которая использует метод max() для поиска наибольшего из двух кругов
// и наибольшего из двух прямоугольников.
public abstract  class GeometricObjects implements Comparable<GeometricObjects> {
    protected boolean isFilled = false;
    protected String color = "Белый";
    protected java.util.Date dateOfCreation;
    protected GeometricObjects(){
        this.dateOfCreation = new Date();
    }
    public void setColor(String color){
        this.color = color;
    }
    public void setFilling(boolean isFilled){
        this.isFilled = isFilled;
    }
    public boolean isFilled() {
        return isFilled;
    }

    public String getColor() {
        return color;
    }

    protected abstract double getArea();


    protected Date getDateOfCreation() {
        return new Date(dateOfCreation.getTime());
    }
    @Override
    public int compareTo(GeometricObjects obj){

            if(obj.getArea() > this.getArea()) {
                return -1;
            }
            else if (obj.getArea() < this.getArea()){
                return 1;
            }
            return 0;
    }
    public static GeometricObjects max(GeometricObjects o1, GeometricObjects o2) {
        if (o1.compareTo(o2) > 0)
            return o1;
        else
            return o2;

    }
    public abstract String toString();
}
