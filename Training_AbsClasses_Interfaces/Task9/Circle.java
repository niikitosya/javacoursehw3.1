package Training_AbsClasses_Interfaces.Task9;

import java.awt.image.CropImageFilter;

public class Circle extends GeometricObjects{
    private final static double PI = Math.PI;
    protected double radius;
    public Circle(double radius){
        super();
        this.radius = radius;
    }
    public int compareTo(Circle in){
        if (in.getArea() > this.getArea()){
            return -1;
        }
        else if(in.getArea() < this.getArea()){
            return 1;
        }
        else {
            return 0;
        }
    }
    public double getArea(){
        return PI*radius*radius/2;
    }
    public double getRadius() {
        return radius;
    }
    public String toString(){
        return ("Круг с радиусом " + this.radius + " и плошадью " + getArea() +
                (super.isFilled ? ". Заливка " + super.color + " Цвета.\n" : ". Заливки нет.\n") + "Дата создания " + super.dateOfCreation.toString());
    }
}
