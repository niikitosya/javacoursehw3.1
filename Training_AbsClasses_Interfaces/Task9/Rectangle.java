package Training_AbsClasses_Interfaces.Task9;

public class Rectangle extends GeometricObjects {
    private double height;
    private double width;
    public Rectangle(int height, int width){
        this.height = height;
        this.width = width;
    }
    @Override
    protected double getArea() {
        return height*width;
    }
 public String toString(){
        return ("Прямоугольник: высота - " + height + ", ширина - " + width + (super.isFilled ? ". Заливка " + super.color + " Цвета.\n" : ". Заливки нет.\n") + "Дата создания " + super.dateOfCreation.toString());
 }
}
