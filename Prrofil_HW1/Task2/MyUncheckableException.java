package Prrofil_HW1.Task2;

import Prrofil_HW1.Task1.MyCheckedException;

public class MyUncheckableException extends RuntimeException {
    public MyUncheckableException(){
        super("Это непроверяемое исключение");
    }
}
