package Prrofil_HW1.Task4;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try(scanner) {
            MyEvenNumber num = new MyEvenNumber(scanner.nextInt());
        }
        catch(InputMismatchException e){
            System.err.println("Пожалуйста, введите целое число");
        }
    }
}