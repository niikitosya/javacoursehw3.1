package Prrofil_HW1.Task4;

public class MyEvenNumber {
    private int n;
    MyEvenNumber(int n) {
        if (n % 2 != 0){
            throw new MyNotEvenNumberException();
        }
        else{
            this.n = n;
        }
    }
}
