package Prrofil_HW1.Task6.FormValidator;

import Prrofil_HW1.Task6.MyExceptions.*;

import java.util.Date;

public final class FormValidator {
    FormValidator(String name, String birthDate, String gender, String height) throws MyException {
       checkName(name);
        checkGender(gender);
        checkBirthDate(birthDate);
        checkHeight(height);
    }
    public static void checkName(String name) throws MyException {
        //проверка имени
        if (name.length() >= 2 && name.length() <= 20 && Character.isUpperCase(name.charAt(0))) {

        } else {
            throw new NameInFormException();
        }
    }
    public static void checkGender(String gender) throws MyException {
        //проверка гендера
        if (gender.equals("Male") || gender.equals("Female")) {
        }
        else
        {
            throw new GenderInFormException();
        }
    }
    public static void checkBirthDate(String birthDate) throws MyException {
        String[] splits = birthDate.split("\\.");
        Date date = new Date(Integer.parseInt(splits[2]) - 1900, Integer.parseInt(splits[1]) - 1,Integer.parseInt(splits[0]));
        if (birthDate.compareTo("01.01.1900") >= 0 ) {
            if (date.getTime() < new Date().getTime()){

            }
        }
        else {
            throw new BirthdateInFormException();
        }

    }
    public static void checkHeight(String height) throws MyException {
        //проверка роста
        try {
            Double.parseDouble(height);
        } catch (NumberFormatException e) {
            throw new DoubleInFormException();
        }
        if (Double.parseDouble(height) >= 0) {

        }
        else {
            throw new HeightInFormException();
        }
    }
}
