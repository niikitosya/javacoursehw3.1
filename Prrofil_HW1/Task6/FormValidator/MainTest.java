package Prrofil_HW1.Task6.FormValidator;

import GenericTain.Task1.User;
import Prrofil_HW1.Task6.MyExceptions.*;

import java.util.Date;
import java.util.Scanner;

public class MainTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите сценарий тестирования" +
                "\n 1. Все ок" +
                "\n 2. Имя начинается не с заглавной буквы" +
                "\n 3. Имя больше допустимого значения" +
                "\n 4. Дата не соответствует требованиям" +
                "\n 5. Введеный пол не соответствует доступным для выбора" +
                "\n 6. Введеный рост не переводится в Double" +
                "\n 7. Рост меньше нуля");
        int n = scanner.nextInt();
        String name = "Nikita";
        String date = "01.01.1998";
        String gender = "Male";
        String height = "1.8";
        switch (n) {
            case 1 -> {
                break;
            }
            case 2 -> name = "nikita";
            case 3 -> name = "adldsdlldsds;l;dslds;sdl;ll;;l;";
            case 4 -> date = "01.01.1870";
            case 5 -> gender = "Bipolar transistor";
            case 6 -> height = "metr c kepkoy";
            case 7 -> height = "-1";
            default -> throw new IllegalStateException("Unexpected value: " + n);
        }
        try {
            new FormValidator(name, date, gender, height);
        } catch (MyException e) {
            System.out.println("Log: " + e.getMessage());
        }
    }
}
