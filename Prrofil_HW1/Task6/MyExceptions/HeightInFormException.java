package Prrofil_HW1.Task6.MyExceptions;

public class HeightInFormException extends MyException{
    public HeightInFormException() {
        super("Ошибка: рост должен быть положительным числом");
    }
}
