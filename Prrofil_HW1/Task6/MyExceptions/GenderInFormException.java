package Prrofil_HW1.Task6.MyExceptions;

public class GenderInFormException extends MyException{
    public GenderInFormException() {
        super("Ошибка: данного пола нет в предложенных для выбора видах");
    }
}
