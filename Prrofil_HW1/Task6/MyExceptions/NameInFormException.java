package Prrofil_HW1.Task6.MyExceptions;

public class NameInFormException extends MyException{
    public NameInFormException() {
        super("Ошибка: длина имени должна находиться в диапазоне от 2 до 20 символов, а также имя должно начинаться с большой буквы");
    }
}
