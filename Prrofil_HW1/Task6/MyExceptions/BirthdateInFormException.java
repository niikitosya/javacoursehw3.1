package Prrofil_HW1.Task6.MyExceptions;

public class BirthdateInFormException extends MyException{
    public BirthdateInFormException() {
        super("Ошибка: Дата рождения не может быть раньше 01.01.1900 и не позже текущей");
    }
}
