package Prrofil_HW1.Task1;

import java.io.IOException;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        try {
            checkException();
        }
        catch (Throwable e ) {
            System.err.println("Log: " + e.getMessage());
        }
    }
    public static void checkException() throws MyCheckedException {
        throw new MyCheckedException();
    }
}
