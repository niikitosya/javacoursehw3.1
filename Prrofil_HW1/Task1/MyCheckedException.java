package Prrofil_HW1.Task1;

public class MyCheckedException extends Throwable {
    MyCheckedException(){
       super("Это проверяемое исключение");
    }
    MyCheckedException(Throwable e){
        super("Это проверяемое исключение", e);
    }
}
