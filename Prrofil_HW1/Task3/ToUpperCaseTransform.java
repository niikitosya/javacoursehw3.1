package Prrofil_HW1.Task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class ToUpperCaseTransform {
    private static final String FILE_DESTINATION = "/Users/nikita/IdeaProjects/JavaCourse/Prrofil_HW1/Task3";
    private static final String INPUT_NAME = "input.txt";
    private static final String OUTPUT_NAME = "output.txt";

    public static void toUpperCaseTransform() throws IOException {
        Scanner scanner = new Scanner(new File(FILE_DESTINATION + "/" + INPUT_NAME));
        Writer wr = new FileWriter(FILE_DESTINATION + "/" + OUTPUT_NAME);
        try(scanner; wr){
            while (scanner.hasNext()){
                wr.write(scanner.nextLine().toUpperCase() + "\n");
            }
        }
        catch (IOException e){
            System.out.println("LOG: " + e.getMessage());
        }
    }
}
