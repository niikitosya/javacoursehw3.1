package JavaHW2_2;

import java.util.Arrays;
import java.util.Scanner;

/*7.	Раз в год Петя проводит конкурс красоты для собак. К сожалению, система хранения участников и оценок
неудобная, а победителя определить надо. В первой таблице в системе хранятся имена хозяев, во второй -
клички животных, в третьей — оценки трех судей за выступление каждой собаки. Таблицы связаны между собой
только по индексу. То есть хозяин i-ой собаки указан в i-ой строке первой таблицы, а ее оценки — в i-ой строке
третьей таблицы. Нужно помочь Пете определить топ 3 победителей конкурса.

На вход подается число N — количество участников конкурса. Затем в N строках переданы имена хозяев.
После этого в N строках переданы клички собак. Затем передается матрица с N строк, 3 вещественных числа в каждой
— оценки судей. Победителями являются три участника, набравшие максимальное среднее арифметическое по оценкам
3 судей. Необходимо вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.

Гарантируется, что среднее арифметическое для всех участников будет различным.
*/
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String a = scanner.nextLine();
        String[] dogs = new String[n];
        String[] members = new String[n];

        for(int i = 0;i <members.length; i++) {
            members[i] = scanner.nextLine();
        }

        for(int i = 0;i <dogs.length; i++) {
            dogs[i] = scanner.nextLine();
        }

        double[] equation = new double[n];
        int[][] votes = new int[n][3];
        int[] numbers = new int[n];
        for(int i = 0; i < votes.length;i++) {
            for(int j = 0; j < votes[0].length; j++){
                votes[i][j] = scanner.nextInt();
                equation[i] += votes[i][j];
            }
            numbers[i] = i;
        }
            for (int i = 0; i < equation.length;i++){
                equation[i] /=3;
            }
        sort(numbers, equation);
        for(int i = 0; i < 3; i++){
            System.out.println(members[numbers[i]] +": " + dogs[numbers[i]] + ", " + equation[i]);
        }
    }
    public static void sort(int[] n, double[] k) {
        int nn;
        double kk;
        for (int i = 0; i < k.length; i++) {
            for (int j = i + 1; j < k.length; j++) {
                if (k[i] < k[j]) {
                  nn = n[j];
                  kk = k[j];
                  k[j] = k[i];
                  n[j] = n[i];
                  n[i] = nn;
                  k[i] = kk;
                }
            }

        }
    }
}
