package JavaHW2_2;

import java.util.Scanner;

/*На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.

Необходимо вывести true, если она является симметричной относительно побочной диагонали, false иначе.

Побочной диагональю называется диагональ, проходящая из верхнего правого угла в левый нижний.
* */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int[][] array = new int[k][k];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        boolean symetric = true;
        for (int i = 0; i < array.length; i++) {
            for (int j = array.length - 1; j >= i; j--) {
                if (array[i][j] != array[array.length - 1 - j][array.length - 1 - i]) {
                    symetric = false;
                }

            }

        }
        System.out.println(symetric);
    }
}