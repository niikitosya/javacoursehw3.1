package JavaHW2_2;

import java.util.Scanner;

/*На вход передается N — количество столбцов в двумерном массиве и M — количество строк.
Затем сам передается двумерный массив, состоящий из натуральных чисел.

Необходимо сохранить в одномерном массиве и вывести на экран минимальный элемент каждой строки.
*/
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int[] results = new int[b];
        int[][] array = new int[a][b];

        for (int i = 0; i < array[0].length; i++) {

            for(int j = 0; j < array.length;j++) {

                array[j][i] = scanner.nextInt();
                if (j == 0) {
                  results[i] = array[j][i];
                }
                if (array[j][i] < results[i]) {
                    results[i] = array[j][i];
                }

            }
            }
        for (int i = 0; i < results.length; i++) {
            if (i == 0) {
                System.out.print(results[i]);
            }
            else System.out.print(" " + results[i]);

        }
    }
}
