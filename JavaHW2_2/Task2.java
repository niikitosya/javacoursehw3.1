package JavaHW2_2;

import java.util.Scanner;

/* На вход подается число N — количество строк и столбцов матрицы. Затем в последующих двух строках подаются
координаты X (номер столбца) и Y (номер строки) точек, которые задают прямоугольник.

Необходимо отобразить прямоугольник с помощью символа 1 в матрице, заполненной нулями (см. пример) и вывести
всю матрицу на экран.
*/
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int l = scanner.nextInt();
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int d = scanner.nextInt();
        int[][] array = new int[l][l];

        filler(array, a, b, c, d);
        for (int i = 0; i < l; i++) {
            System.out.print(array[i][0]);
            for (int j = 1; j < array.length; j++) {
                System.out.print(" " +array[i][j]);
        }
            System.out.println();
        }
    }
    public static void filler(int[][] array , int a, int b, int c, int d) {
        for (int i = b; i <= d; i ++) {
            for (int j = a; j <= c; j++) {
                if(i == b  || i == d) {
                    array[i][j] = 1;
                }
                else if (j == a || j == c) {
                    array[i][j] = 1;
                }
            }
        }
    }
}
