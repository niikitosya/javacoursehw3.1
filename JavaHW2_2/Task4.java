package JavaHW2_2;

import java.util.Scanner;

/*
* На вход подается число N — количество строк и столбцов матрицы. Затем передается сама матрица,
* состоящая из натуральных чисел. После этого передается натуральное число P.

Необходимо найти элемент P в матрице и удалить столбец и строку его содержащий
* (т.е. сохранить и вывести на экран массив меньшей размерности). Гарантируется, что искомый элемент
* единственный в массиве.
*/
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int[][] array = new int[a][a];
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array.length; j++){
                array[i][j] = scanner.nextInt();
            }
        }
        int m = 0, s = 0;
        int k = scanner.nextInt();
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array.length; j++){
                if (k == array[i][j]){
                    m = i;
                    s = j;
                }
            }
        }
        for(int i = 0; i < array.length; i++){
            if(i == m) continue;
            System.out.print(array[i][0]);
            if (m == 0) {
                System.out.print(array[i][1]);
            }
            for(int j = 1; j < array.length; j++){
                if(j == s) continue;
                System.out.print(" " + array[i][j]);
            }
            System.out.println();

        }
    }
}
