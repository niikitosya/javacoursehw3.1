package JavaHW2_2;
//10.	На вход подается число N. Необходимо вывести цифры числа справа налево.
// Решить задачу нужно через рекурсию.
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int  chislo = scanner.nextInt();
        String ch = String.valueOf(chislo);
        String c = String.valueOf(chislo);
        int[] array = new int[ch.length()];
        int k =10;
        for(int i = 0; i <array.length; i++) {
            array[i] = chislo % k;
            chislo/=10;
        }
        vivod(array, array.length -1  );
    }

    public static void vivod (int[] arr, int last) {
        if (last == 1) {
            System.out.print(arr[0] + " " + arr[1]);
            return;
        }

        vivod(arr, last - 1);
        System.out.print(" " + arr[last]);
        return;
    }
}
