package JavaHW2_2;
//9.	На вход подается число N. Необходимо вывести цифры числа слева направо.
// Решить задачу нужно через рекурсию.
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int  chislo = scanner.nextInt();
        String ch = String.valueOf(chislo);
        int[] array = new int[ch.length()];
        int k =10;
        for(int i = 0; i <array.length; i++) {
            array[i] = chislo % k;
            chislo/=10;
        }
        for (int i = 0; i < array.length;i++) {
            System.out.println(array[i]);
        }
        vivod(array, 0 );
    }

    public static void vivod (int[] arr, int last) {
        if (arr.length - last == 2) {
            System.out.print(arr[arr.length - 1] + " " + arr[arr.length - 2]);
            return;
        }

        vivod(arr, last + 1);
        System.out.print(" " + arr[last]);
        return;
    }
}