package JavaHW2_2;
//8.	На вход подается число N.
// Необходимо посчитать и вывести на экран сумму его цифр.
// Решить задачу нужно через рекурсию.

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int  chislo = scanner.nextInt();
        String ch = String.valueOf(chislo);
        int[] array = new int[ch.length()];
        int k =10;
        for(int i = 0; i <array.length; i++) {
            array[i] = chislo%k;
            chislo/=10;
        }
        for (int i = 0; i < array.length;i++) {
            System.out.println(array[i]);
        }
        System.out.println(sum(array, 0));

    }
    public static int sum (int[] arr, int first) {
        if (arr.length - first == 2)  return arr[first] + arr[first+1];
        return arr[first] + sum(arr, ++first);
    }
}
