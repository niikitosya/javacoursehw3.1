package HW3_1;

public class Task5 {
    public static void main(String[] args) {
        DaysOfWeek daysofweek = new DaysOfWeek();
        DaysOfWeek[] days1 = daysofweek.setArrayDays();
        DaysOfWeek[] days2 = new DaysOfWeek[7];
        daysofweek.setArrayDays(days2);
        daysofweek.showDays(days1);
        daysofweek.showDays(days2);
    }

}

class DaysOfWeek {
    final static String[] daysName = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    private byte num;
    private String day;

    DaysOfWeek() {

    }
        public void getDay (String day){
            this.day = day;
        }

        public void getNum ( byte num){
            this.num = num;
        }
        public void setArrayDays (DaysOfWeek[]days){
            for (byte i = 0; i < days.length; i++) {
                days[i] = new DaysOfWeek();
                days[i].day = daysName[i];
                days[i].num = (byte) (i + 1);
            }
        }
        public DaysOfWeek[] setArrayDays () {
            DaysOfWeek[] outDays = new DaysOfWeek[7];
            for (byte i = 0; i < outDays.length; i++) {
                outDays[i] = new DaysOfWeek();
                outDays[i].day = daysName[i];
                outDays[i].num = (byte) (i + 1);
            }
            return outDays;
        }

        public void showDays (DaysOfWeek[]days){
            for (int i = 0; i < days.length; i++) {
                System.out.println(days[i].num + " " + days[i].day);
            }
        }

    }