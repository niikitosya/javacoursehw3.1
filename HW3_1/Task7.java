package HW3_1;
/* еализовать класс TriangleChecker, статический метод которого принимает три длины сторон треугольника и возвращает
true, если возможно составить из них треугольник, иначе false. Входные длины сторон треугольника — числа типа double.
Придумать и написать в методе main несколько тестов для проверки работоспособности класса (минимум один тест на результат true и один на результат false)*/

public class Task7 {
    public static void main(String[] args) {
        System.out.println("Тест 1: передадим стороны, которые удволетворяю всем условиям");
        System.out.println(TriangleChecker.IsTriangle(2, 4, 5));

        System.out.println("Тест 2: Передаем объекту стороны, при которых не выполняются условия");
        System.out.println(TriangleChecker.IsTriangle(5, 100.1, 2.2));
        System.out.println("Тест 3: передадим отрицальные стороны");
        System.out.println(TriangleChecker.IsTriangle(-2, 5, 9));

        System.out.println("Тест 5: Отдельная проверка при передаче нулевой длины стороны, т.к. тип сторон boolean");
        System.out.println(TriangleChecker.IsTriangle(0, 0, 0));
    }
}
final class TriangleChecker {

    public static boolean IsTriangle(double a, double b, double c){
        if(a > 0 && b > 0 && c > 0){
            if (a + b > c && c + b > a && a + c > b){
                return true;
            }
        }
        else{
            System.out.println("Введены недопустимые значения сторон");

        }
        return false;
    }
 }

