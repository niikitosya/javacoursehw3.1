package HW3_1;

import javax.swing.*;

public class Task6 {
    public static void main(String[] args) {
        AmazingString a =new AmazingString(" Привет");
        AmazingString b = new AmazingString(new char[]  {' ',' ',' ','и' ,'в','е','т',' '});
        a.removeLeadSpaces();
        b.removeLeadSpaces();
        System.out.print(b.returnCharAtIndex(4));

    }
}


class AmazingString {
    private char[] array;
    AmazingString() {

        }
    AmazingString(String str){
        array = toCharArray(str);
    }
    AmazingString(char[] array) {
        this.array = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            this.array[i] = array[i];
        }
    }
    public void setAmazingString(char[] array){
        this.array = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            this.array[i] = array[i];
        }
    }
    public void setAmazingString(String str){
        this.array = toCharArray(str);
    }

    public char returnCharAtIndex(int k) {
        if (k >= array.length || k < 0) {
            System.out.println("Введен неверный индекс или индекс находится за пределами строки");
            return ' ';
        }
        else {
            return this.array[k];
        }
    }

    public int returnLength() {
        return this.array.length;
    }

    public void showString() {
        for (int i = 0; i < this.array.length-1; i++) {
            System.out.print(this.array[i]);
        }
        System.out.println(this.array[array.length -1 ]);
    }

    public boolean stringMatches(char[] inArray) {
        boolean out = false;
        int k = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (inArray[k] == this.array[i]) {
                if (k == inArray.length - 1) {
                    out = true;
                    break;
                } else {
                    k++;
                }
            } else {
                k = 0;
            }
        }
        return out;
    }

    public boolean stringMatches(String inLine) {
        char[] inArray = toCharArray(inLine);
        return this.stringMatches(inArray);
    }
    public char[] toCharArray(String str) {
        char[] out = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            out[i] = str.charAt(i);
        }
        return out;
    }
    public void removeLeadSpaces(){
        int k = 0;
        for (char c : this.array) {
            if (c == ' ') {
                k++;
            } else {
                break;
            }

        }
        char[] newArray = new char[this.array.length - k];
        for (int i = 0; i < newArray.length;i++) {
            newArray[i] = array[i+k];
        }
        this.array = newArray;
    }
    public void showWithRemoveLeadSpaces() {//вариант для вывода, не меняя содержимого строки
        boolean isLead = true;
        for (char c : this.array) {
            if (c == ' ' && isLead) {
                continue;
            } else {
                System.out.print(c);
                isLead = false;
            }
        }
        System.out.println();
    }
    public void reverseString(){
        for (int i = this.array.length - 1; i >= 0; i--){
                System.out.print(this.array[i]);
            }
        System.out.println();
        }
    }