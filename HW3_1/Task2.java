package HW3_1;

public class Task2 {
    public static void main(String[] args) {
        Student one = new Student("Nikita", "Yakimchuk");
        one.setGrades(new int[]{5, 5, 5, 4, 5});
        Student two = new Student();
        two.setName("Uliana");
        two.setSurname("Alecseeva");
        System.out.println(one.getMean());
        one.newGrade(2);
        one.newGrade(new int[] {1, 1, 1, 1, 1, 1, 1,1, 1, 2, 1});
        int[] grade;
        grade = one.getGrades();
        System.out.println(grade[9]);
        System.out.println(grade[8]);
    }
}
class Student {
    private String name;
    private String surname;
    private final int[] grades = new int[10];
    private boolean isFull = false;
    Student() {
    }

    Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getGrades() {
        int[] newGrades = new int[10];
        for(int i = 0; i < newGrades.length; i++){
            newGrades[i] = grades[i];
        }
        return newGrades; //если передавать просто grade то передастся его ссылка
    }

    public void setGrades(int[] grades) {
        if (grades.length > 10) {
            for (int i = 0; i < grades.length; i ++){
                this.grades[i] = grades[i + (grades.length - this.grades.length)];
            }
        }
        else {
            for (int i = 0; i < this.grades.length; i ++){
                if (grades.length -1 < i) {
                    this.grades[i] = -1;
                }
                else {
                    this.grades[i] = grades[i];
                }
            }
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void newGrade(int grades) {

        for (int i = 1; i < this.grades.length; i++) {
            this.grades[i - 1] = this.grades[i];
        }
        this.grades[this.grades.length - 1] = grades;
    }
    public void newGrade(int[] grades) {
        int k = 0; //какой элемен после дозаполненияч массива оценок является нулевым
       if (!isGradesArrayFull(this.grades)) {
           for (int i = getFirstEmptyCell(this.grades); i < this.grades.length; i++){
               this.grades[i] = grades[k];
               if (k == grades.length) {
                   break;
               }
               k++;
               }
           }
       if (grades.length - k <= 0){
           return;
       }
       if (grades.length - k == 1) {
           this.newGrade(grades[k +1]);
       }
        if (grades.length - k == 10) {
            for (int i = 0; i < grades.length - k; i++) {
                this.grades[i] = grades[i + k];
            }
        }
        else if (grades.length - k < 10) {

            for (int i = 0; i < grades.length - k; i++) {
                this.grades[i] = this.grades[this.grades.length - grades.length + k];
            }
            for (int i = 0; i <grades.length; i++) {
                this.grades[i + this.grades.length - grades.length + k] = grades[i + k];
            }
        }
        else {
            for(int i = 0; i < this.grades.length; i++) {
                this.grades[i] = grades[i + (grades.length - this.grades.length)];
            }
        }
    }
    private int getFirstEmptyCell(int[] array) { // ищет самый первый пустой элемент массива оценок
        int s = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == -1 ) {
                s = i;
                break;
            }

        }
        return s;
    }
    private boolean isGradesArrayFull(int[] array) { //проверка, заполнен ли массив полностью
        boolean out = true;
        for (int e: array){
            if (e == -1){
                out = false;
            }
        }
        return out;
    }


    public double getMean() {
        int sum = 0;
        int k = 0;
        for (int i = 0; i < this.grades.length; i++) {
            if (this.grades[i] == -1) {
                continue;
            }
          sum += this.grades[i];
            k++;
        }
        return (int)(sum * 100 / k) / 100.0;
    }
}