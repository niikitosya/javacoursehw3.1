package HW3_1;

public class Task8 {
    public static void main(String[] args) {
        Atm a = new Atm();
        a.setDollarsPerRubles(0.002);
        System.out.println(a. dollarsConvert(30000));
    }}
class Atm {
    private double dollarsPerRubles;
    private double rublesPerDollars;
    private boolean isAddRub =false;
    private boolean isAddUsd = false;
    private static int countOfDuplicates =0;
    Atm(){
        countOfDuplicates++;
    }
    Atm(double dollarsPerRubles, double rublesPerDollars){
        if (dollarsPerRubles > 0 && rublesPerDollars > 0) {
            this.rublesPerDollars = rublesPerDollars;
            this.dollarsPerRubles = dollarsPerRubles;
            isAddRub =true;
            isAddUsd = true;
        }
        else {
            System.out.println("Значеня курса валют должно быть больше 0");
        }
        countOfDuplicates++;
    }
    public void setDollarsPerRubles(double dollarsPerRubles){
        if (dollarsPerRubles > 0) {
            this.dollarsPerRubles = dollarsPerRubles;
            isAddUsd = true;
        }
        else {
            System.out.println("Значеня курса валют должно быть больше 0");
        }
    }
    public void setRublesPerDollars(double rublesPerDollars){
        if (dollarsPerRubles > 0) {
            this.rublesPerDollars = rublesPerDollars;
            isAddRub =true;
        }
        else {
            System.out.println("Значеня курса валют должны быть больше 0");
        }
    }
    public int getCountOfDuplicates() {
        return countOfDuplicates;
    }
    public double dollarsConvert(double dollars) {
        if (isAddRub) {
            return ((int) (dollars * this.rublesPerDollars * 100)) / 100.0;
        }
        else {
            System.out.println("Не задано значение курса доллар/рубль");
            return -1;
        }
    }

    public double rublesConvert(double rubles) {
        if (isAddUsd) {
            return ((int) (rubles * this.dollarsPerRubles * 100)) / 100.0;
        }
        else {
            System.out.println("Не задано значение курса рубль/доллар");
            return -1;
        }

    }
}

