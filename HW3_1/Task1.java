package HW3_1;
/* 1.	Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
●	sleep() — выводит на экран “Sleep”
●	meow() — выводит на экран “Meow”
●	eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.

*/
public class Task1 {
    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            Cat.status();
        }
    }
}
class Cat {
    private static void sleep(){
        System.out.println("Sleep");
    }
    private static void meow() {
        System.out.println("Meow");
    }
    private static void eat() {
        System.out.println("Eat");
    }
    public static void status() {
        int k = (int)(Math.random()*3);
        switch (k) {
            case 0:
                sleep();
                break;
            case 1:
                meow();
                break;
            case 2:
                eat();
                break;
        }
    }

}
