package HW3_1;

public class Task4 {
    public static void main(String[] args) {
        TimeUnit a = new TimeUnit(23, 5, 10);
        a.getTime();
        a.getTime12hFormat();
        a.addTime(2, 4, 650);
        a.getTime12hFormat();
    }
}
class TimeUnit {
    private int hours;
    private int mins;
    private int seconds;
    TimeUnit(){

    }
    TimeUnit(int hours) {
        if (hours > 23) {
            System.out.println("Invalid date. Warning, time have a default value (00:00:00).");
        } else {
            this.hours = hours;
            this.mins = 0;
            this.seconds = 0;
        }
    }

    TimeUnit(int hours, int mins) {
        if (hours > 23 || mins > 59) {
            System.out.println("Invalid date. Warning, time have a default value (00:00:00).");
        } else {
            this.hours = hours;
            this.mins = mins;
            this.seconds = 0;
        }
    }

    TimeUnit(int hours, int mins, int seconds) {
        if (hours > 23 || mins > 59 || seconds > 59) {
            System.out.println("Invalid date. Warning, time have a default value (00:00:00).");
        }
        else {
            this.hours = hours;
            this.mins = mins;
            this.seconds = seconds;
        }
    }

    public void getTime() {

        if (this.hours < 10) {
            System.out.print(0);
        }
        System.out.print(this.hours + ":");
        if (this.mins < 10) {
            System.out.print(0);
        }
        System.out.print(this.mins + ":");
        if (this.seconds < 10) {
            System.out.print(0);
        }
        System.out.println(this.seconds);
    }
        public void getTime12hFormat(){

            if (this.hours < 12){
                if (this.hours < 10) {
                    System.out.print(0);
                }
                System.out.print(this.hours + ":");
            } else
            {
                if (this.hours - 12 < 10) {
                    System.out.print(0);
                }
                System.out.print((this.hours - 12) + ":");
            }
            if (this.mins < 10) {
                System.out.print(0);
            }
            System.out.print(this.mins + ":");
            if (this.seconds < 10) {
                System.out.print(0);
            }
            System.out.print(this.seconds);
            if (hours > 12) {
                System.out.println( " pm" );
            }
            else {
                System.out.println(" am");
            }

        }
        public void addTime(int hours, int mins, int seconds) {
            if (hours < 24 && mins < 60 && seconds < 60) {
                this.seconds += seconds;
                if (this.seconds + seconds >= 60) {
                    this.mins++;
                    this.seconds -= 60;
                }
                this.mins += mins;
                if (this.mins >= 60) {
                    this.hours++;
                    this.mins -= 60;
                }
                this.hours += hours;
                if (this.hours >= 24) {
                    this.hours -= 24;
                }
            }
            else {
                System.out.println("Invalid date.");
            }
        }
}