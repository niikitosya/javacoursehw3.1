package HW3_1;

public class Task3 {
    public static void main(String[] args) {
        Student[] a = new Student[2];
        a[0] = new Student("Nikita", "Yakimchuk");
        a[0].setGrades(new int[]{3, 4, 4, 4 ,5 ,5, 3, 5, 5,4});
        a[1] = new Student("Uliana", "Alecseeava");
        a[1].setGrades(new int[]{3, 4, 4, 4 ,5 ,5, 5, 5, 5,4});
        StudentService service = new StudentService();
        System.out.println(service.sortByName(a)[0].getName());

    }
}
class StudentService {
    StudentService(){

    }
    public Student bestStudent(Student[] students) {
        Student bestStudent = students[0];
        for (int i = 1; i < students.length ; i++) {
                if (students[i].getMean() > bestStudent.getMean()) {
                    bestStudent = students[i];
                }
            }
        return bestStudent;
        }
        public Student[] sortByName(Student[] students) {
        Student outStudent;
        Student[] sortStudents = new Student[students.length];
        for (int i = 0; i < students.length; i++) {
            sortStudents[i] = students[i];
        }
        for (int i = 0; i < students.length; i++) {
            for (int j = i + 1; j < students.length;j++) {
                if (sortStudents[i].getSurname().charAt(0) > sortStudents[j].getSurname().charAt(0)) {
                    outStudent = sortStudents[i];
                    sortStudents[i] = sortStudents[j];
                    sortStudents[j] = outStudent;
                }
            }
        }
        return sortStudents;
        }
    }
