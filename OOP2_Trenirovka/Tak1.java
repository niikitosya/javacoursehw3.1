package OOP2_Trenirovka;

import java.util.InputMismatchException;

public class Tak1 {
    public static void main(String[] args) {
    Calendar.showMonthParams();
    }
}
final class Calendar{
    final private static String[] NAME_OF_MONTH = {"январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};
    final private static int[] DAYS_IN_MONTH = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public static void showMonthParams(){
        java.util.Scanner scanner = new java.util.Scanner(System.in);
        try {
            int k = scanner.nextInt() - 1;
            System.out.println();
            System.out.print(NAME_OF_MONTH[k] + " - в нем " + DAYS_IN_MONTH[k]);
            if (DAYS_IN_MONTH[k] % 10 == 1) {
                System.out.print(" день.");
            }
            else{
                System.out.println("дней.");
            }
        }
        catch (ArrayIndexOutOfBoundsException bug){
            System.out.println("Введено недопустимое число ");
        }
        catch (InputMismatchException bug){
            System.out.println("Пожалуйста, введите целое число");
        }
    }
}

